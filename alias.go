package operatorframework

import (
	_ "embed"

	"gitlab.com/pursultani/operator-framework/pkg/resource"
)

type (
	// Objects
	Objects       = resource.Objects
	ObjectEditors = resource.ObjectEditors

	// Inventories
	AggregateInventory = resource.AggregateInventory
	ChartFilters       = resource.ChartFilters
	ChartInventory     = resource.ChartInventory
	DynamicInventory   = resource.DynamicInventory
	StaticInventory    = resource.StaticInventory

	// Values
	Values        = resource.Values
	ValueProvider = resource.ValueProvider
)

var (
	//go:embed VERSION
	Version string

	// Objects
	RejectObject = resource.RejectObject

	// Settings
	SetChartDirectory     = resource.SetChartDirectory
	SetInventoryCacheSize = resource.SetInventoryCacheSize
	SetInventoryCacheTTL  = resource.SetInventoryCacheTTL
)
