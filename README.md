# GitLab Operator Framework

GitLab Operator Framework is an opinionated framework for building [Kubernetes Operators](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/).
It is built on top of [`controller-runtime`](https://github.com/kubernetes-sigs/controller-runtime) and is designed to
work with [Operator Framework](https://operatorframework.io/). It offers a DSL-like syntax for declarative description
of what an Operator does and how it works.
