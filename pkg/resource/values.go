package resource

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Values holds an arbitrary tree-like data structure, such as parsed JSON or
// YAML. It can be used to represent Helm-like values.
//
// The convenient getter methods can retrieve the values with dot-separated key,
// for example `x.y.z`. The key format does not support list indices.
type Values map[string]interface{}

// GetValue retrieves the value that is addressed by a dot-separated key, e.g.
// `x.y.z`. The key format does not support list index.
//
// It will return an error, if the key does not exist or can not traversed, e.g.
// on leaf nodes of the tree.
func (v Values) GetValue(key string, defaultValue ...interface{}) (interface{}, error) {
	cursor := v
	keyElements := []string{}

	if key != "" {
		keyElements = strings.Split(key, ".")
	}

	var target interface{} = cursor
	for idx, elm := range keyElements {
		target, hasTarget := cursor[elm]

		if !hasTarget || target == nil {
			if idx < len(keyElements)-1 {
				return nil, errors.Errorf("missing element at %s for %s key", elm, key)
			}
		}

		if targetAsMap, ok := target.(map[string]interface{}); ok {
			cursor = targetAsMap
		} else {
			if idx < len(keyElements)-1 {
				return nil, errors.Errorf("leaf element at %s for %s key", elm, key)
			}
		}
	}

	return target, nil
}

// GetString retrieves the value of type `string` that is addressed by a
// dot-separated key, e.g. `x.y.z`. The key format does not support list index.
//
// If the key does not exist it will return the optional `defaultValue` or an
// empty string. When the key exists but its type is not `string` it tries to
// format it as `string`. If it fails it will return either the `defaultValue`
// or an empty string.
//
// Use `HasKey` method if you need to check existence of the `key`.
func (v Values) GetString(key string, defaultValue ...string) string {
	defaultValueToUse := ""
	if len(defaultValue) > 0 {
		defaultValueToUse = defaultValue[0]
	}

	if val, err := v.GetValue(key); err == nil {
		if strVal, isStr := val.(string); isStr {
			return strVal
		}
		/* You are at the mercy of fmt.Sprintf */
		return fmt.Sprintf("%s", val)
	}
	return defaultValueToUse
}

// GetBool retrieves the value of type `bool` that is addressed by a
// dot-separated key, e.g. `x.y.z`. The key format does not support list index.
//
// If the key does not exist it will return the optional `defaultValue` or an
// `false`. When the key exists but its type is not `bool` it will return
// either the `defaultValue` or `false`.
//
// Use `HasKey` method if you need to check existence of the `key`.
func (v Values) GetBool(key string, defaultValue ...bool) bool {
	defaultValueToUse := false
	if len(defaultValue) > 0 {
		defaultValueToUse = defaultValue[0]
	}

	if val, err := v.GetValue(key); err == nil {
		if boolVal, isBool := val.(bool); isBool {
			return boolVal
		}
	}
	return defaultValueToUse

}

// HasKey checks if a specific key exists. The `key` is in dot-separated format
// and it does not support list index.
func (v Values) HasKey(key string) bool {
	if _, err := v.GetValue(key); err == nil {
		return true
	}
	return false
}

// ValueProvider provides a mechanism to adapt Custom Resources and bind them
// to a tree-like data structure. This structure can be consumed by any other
// component in the framework.
type ValueProvider interface {
	// Origin returns the reference to the underlying Custom Resource.
	Origin() client.Object

	// Name returns the fully qualified name of the underlying Custom Resource.
	Name() types.NamespacedName

	// Values returns a mapping of a Custom Resource specification.
	//
	// You should not make any assumption about how the mapping is done. An
	// implementation may choose to do it eagerly or lazily. However, a mapping
	// must be one-way, meaning that the changes that are applied to it must not
	// propagate to the resource.
	//
	// The provider does not watch the resource. It is the responsibility of the
	// caller to notify the provider upon changes. The provider may be able to
	// re-map the changed resources but it is not obligated to support it.
	Values() Values

	// Hash returns a unique hash for of the underlying Custom Resource.
	//
	// You should not make any assumption about when the hash calculated. An
	// implementation may choose to do it eagerly or lazily. However it must
	// guarantee that, after the completion of mapping, at any point in time
	// it returns a correct value that does not change per mapping.
	Hash() string
}

// EasyHash offers a default implementation for `ValueProvider.Hash`. It uses
// the original Custom Resource to calculate the hash value.
//
// Limitations: The hash uses `.metadata.uid` and `.metatdata.generation`. For
// those values to be valid, the resource must be populated with Kubernetes API,
// for example the resource must be populated with `Client.Get`. Also
// `generation` does not change when a resource `.metadata` or `.status` change.
// So as long as the changes in resource `metadata` do not change resource `uid`
// the hash does not change.
func EasyHash(origin client.Object) string {
	return fmt.Sprintf("%s.%d",
		origin.GetUID(), origin.GetGeneration())
}
