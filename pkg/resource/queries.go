package resource

import (
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

/* TODO: document */
type Labels map[string]string

/* TODO: document */
type Query interface {
	// Test checks if the object fits the criteria of the query.
	Test(object client.Object) (bool, error)
}

/* TODO: document */
type Queries []Query

/* TODO: document */
func (o Objects) Select(query Query) (Objects, error) {
	return evaluteQuery(o, query, true)
}

/* TODO: document */
func (o Objects) Reject(query Query) (Objects, error) {
	return evaluteQuery(o, query, false)
}

/* TODO: document */
type TypeMetaQuery struct {
	schema.GroupVersionKind
}

/* TODO: document */
func (q *TypeMetaQuery) Test(object client.Object) (bool, error) {
	gvk := object.GetObjectKind().GroupVersionKind()
	result := gvkTest(gvk.Kind, q.Kind) && gvkTest(gvk.Group, q.Group) &&
		gvkTest(gvk.Version, gvk.Version)
	return result, nil
}

/* TODO: document */
type ObjectMetaQuery struct {
	Names     []string
	Labels    Labels
	UseRegExp bool
}

/* TODO: document */
func (q *ObjectMetaQuery) Test(object client.Object) (bool, error) {
	if len(q.Names) > 0 {
		for _, name := range q.Names {
			outcome, err := extStrComp(name, object.GetName(), q.UseRegExp)
			if err != nil {
				return false, err
			}
			if outcome {
				return true, nil
			}
		}
	}
	if len(q.Labels) > 0 {
		for key, value := range q.Labels {
			observedValue, isPresent := object.GetLabels()[key]
			if isPresent {
				outcome, err := extStrComp(value, observedValue, q.UseRegExp)
				if err != nil {
					return false, err
				}
				if outcome {
					return true, nil
				}
			}
		}
	}
	return false, nil
}

/* TODO: document */
type QueryComposition int

const (
	Any  QueryComposition = 0
	All  QueryComposition = iota
	None QueryComposition = iota
)

/* TODO: document */
type CompositeQuery struct {
	Queries     Queries
	Composition QueryComposition
}

/* TODO: document */
func (q *CompositeQuery) Test(object client.Object) (bool, error) {
	var x, y bool
	switch q.Composition {
	case All:
		x = false
		y = false
	case None:
		x = true
		y = false
	default:
		x = true
		y = true
	}
	for _, query := range q.Queries {
		outcome, err := query.Test(object)
		if err != nil {
			return outcome, err
		}
		if outcome == x {
			return y, nil
		}
	}
	return !y, nil
}

/* TODO: document */
func QueryByKind(kind string) Query {
	return &TypeMetaQuery{
		GroupVersionKind: schema.GroupVersionKind{
			Kind: kind,
		},
	}
}

/* TODO: document */
func QueryByKindAndNames(kind string, names ...string) Query {
	return &CompositeQuery{
		Queries: Queries{
			QueryByKind(kind),
			&ObjectMetaQuery{
				Names: names,
			},
		},
		Composition: All,
	}
}

/* TODO: document */
func QueryByKindAndNamesRegExp(kind string, regexps ...string) Query {
	return &CompositeQuery{
		Queries: Queries{
			QueryByKind(kind),
			&ObjectMetaQuery{
				Names:     regexps,
				UseRegExp: true,
			},
		},
		Composition: All,
	}
}

/* TODO: document */
func QueryByKindAndLabels(kind string, labels Labels) Query {
	return &CompositeQuery{
		Queries: Queries{
			QueryByKind(kind),
			&ObjectMetaQuery{
				Labels: labels,
			},
		},
		Composition: All,
	}
}

/* TODO: document */
func QueryByComponent(components ...string) Query {
	labelValue, useRegExp := makeComponentLabelCriterion(components)
	return &ObjectMetaQuery{
		Labels: Labels{
			cfgComponentLabelKey: labelValue,
		},
		UseRegExp: useRegExp,
	}
}

/* TODO: document */
func QueryByKindAndComponent(kind string, components ...string) Query {
	return &CompositeQuery{
		Queries: Queries{
			QueryByKind(kind),
			QueryByComponent(components...),
		},
		Composition: All,
	}
}
