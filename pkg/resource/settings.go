package resource

import (
	"time"
)

var (
	cfgChartDirectory                   = ""
	cfgInventoryCacheSize               = 0
	cfgInventoryCacheTTL  time.Duration = 5 * time.Minute
	cfgComponentLabelKey                = "app"
	cfgChartDisableHooks                = false
	cfgChartKubeVersion                 = ""
	cfgChartAPIVersions                 = []string{}
)

func setStringCfg(value *string, newValue string) (oldValue string) {
	oldValue, *value = *value, newValue
	return
}

func setStringSliceCfg(value *[]string, newValue []string) (oldValue []string) {
	oldValue, *value = *value, newValue
	return
}

func setBoolCfg(value *bool, newValue bool) (oldValue bool) {
	oldValue, *value = *value, newValue
	return
}

func setIntCfg(value *int, newValue int) (oldValue int) {
	oldValue, *value = *value, newValue
	return
}

func setDurationCfg(value *time.Duration, newValue time.Duration) (oldValue time.Duration) {
	oldValue, *value = *value, newValue
	return
}

/* TODO: document */
func SetChartDirectory(newValue string) string {
	return setStringCfg(&cfgChartDirectory, newValue)
}

/* TODO: document */
func SetInventoryCacheSize(newValue int) int {
	return setIntCfg(&cfgInventoryCacheSize, newValue)
}

/* TODO: document */
func SetInventoryCacheTTL(newValue time.Duration) time.Duration {
	return setDurationCfg(&cfgInventoryCacheTTL, newValue)
}

/* TODO: document */
func SetComponentLabelKey(newValue string) string {
	return setStringCfg(&cfgComponentLabelKey, newValue)
}

/* TODO: document */
func SetChartDisableHooks(newValue bool) bool {
	return setBoolCfg(&cfgChartDisableHooks, newValue)
}

/* TODO: document */
func SetChartKubeVersion(newValue string) string {
	return setStringCfg(&cfgChartKubeVersion, newValue)
}

/* TODO: document */
func SetChartAPIVersions(newValue []string) []string {
	return setStringSliceCfg(&cfgChartAPIVersions, newValue)
}
