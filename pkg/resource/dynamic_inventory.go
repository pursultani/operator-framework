package resource

import (
	"github.com/patrickmn/go-cache"
)

/* DynamicInventory implementation */

type dynamicInventory struct {
	cache   *cache.Cache
	editors ObjectEditors
	objects Objects
}

func newDynamicInventory(objects Objects, editors ObjectEditors) *dynamicInventory {
	return &dynamicInventory{
		cache:   cache.New(cfgInventoryCacheTTL, cfgInventoryCacheTTL),
		editors: editors,
		objects: objects,
	}
}

func (i *dynamicInventory) get(cacheKey string) Objects {
	cached, isCached := i.cache.Get(cacheKey)
	if isCached && cached != nil {
		result, isObjects := cached.(Objects)
		if isObjects {
			return result
		}
	}
	return nil
}

func (i *dynamicInventory) set(cacheKey string, objects Objects) {
	i.cache.Set(cacheKey, objects, cache.DefaultExpiration)
}

func (i *dynamicInventory) edit(provider ValueProvider) (Objects, error) {
	result := i.objects.DeepCopy()
	for _, obj := range result {
		for _, editor := range i.editors {
			err := editor(obj, provider)
			if err != nil && !isRejectObjectError(err) {
				return result, err
			}
		}
	}
	return result, nil
}
