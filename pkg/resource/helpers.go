package resource

import (
	"fmt"
	"math"
	"regexp"
	"strings"

	"k8s.io/kubectl/pkg/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var (
	decoder = scheme.Codecs.UniversalDeserializer()
)

type errorRejectObject struct {
	object client.Object
}

func (e errorRejectObject) Error() string {
	return fmt.Sprintf("object rejected: %s [%s:%s]",
		e.object.GetObjectKind().GroupVersionKind().String(),
		e.object.GetNamespace(), e.object.GetName())
}

func isRejectObjectError(e error) bool {
	_, ok := e.(errorRejectObject)
	return ok
}

func any(predicates ...ObjectPredicate) ObjectPredicate {
	return func(object client.Object, provider ValueProvider) (bool, error) {
		for _, predicate := range predicates {
			outcome, err := predicate(object, provider)
			if err != nil {
				return outcome, err
			}
			if outcome {
				return true, nil
			}
		}
		return false, nil
	}
}

func all(predicates ...ObjectPredicate) ObjectPredicate {
	return func(object client.Object, provider ValueProvider) (bool, error) {
		for _, predicate := range predicates {
			outcome, err := predicate(object, provider)
			if err != nil {
				return outcome, err
			}
			if !outcome {
				return false, nil
			}
		}
		return true, nil
	}
}

func negate(predicates ...ObjectPredicate) ObjectPredicates {
	result := make(ObjectPredicates, len(predicates))
	for idx, predicate := range predicates {
		result[idx] = func(origin ObjectPredicate) ObjectPredicate {
			return func(object client.Object, provider ValueProvider) (bool, error) {
				outcome, err := origin(object, provider)
				return !outcome, err
			}
		}(predicate)
	}
	return result
}

func filterChartObjects(include, exclude ObjectPredicates) ObjectPredicate {
	filters := make(ObjectPredicates, 1+len(include))
	filters[0] = negate(any(exclude...))[0]
	copy(filters[1:], include)
	return all(filters...)
}

func makeComponentLabelCriterion(components []string) (string, bool) {
	nComponents := len(components)
	labelValue := ""
	useRegExp := false
	if nComponents > 1 {
		/*
		 * This is quick workaround using regular expression for matching
		 * multiple components.
		 */
		labelValue = fmt.Sprintf("^(%s)$", strings.Join(components, "|"))
		useRegExp = true
	} else if nComponents == 1 {
		labelValue = components[0]
	}
	return labelValue, useRegExp
}

const fEulerConstant = 0.57721 /* Euler's constant */

func estimateCapacity(nSize int) int {
	fSize := float64(nSize)
	/*
	 * This is a heuristic that uses an asymptotic estimation of Harmonic
	 * mean of 1 to n (size).
	 *
	 * For calculation see https://mathworld.wolfram.com/HarmonicNumber.html
	 */
	fAvg := fSize / (math.Log(fSize) + fEulerConstant + 1/(2*fSize))
	return int(math.Ceil(fAvg))
}

func evaluteQuery(objects Objects, query Query, outcome bool) (Objects, error) {
	result := make(Objects, 0, estimateCapacity(len(objects)))
	for _, obj := range objects {
		test, err := query.Test(obj)
		if err != nil {
			return result, err
		}
		if test == outcome {
			result = append(result, obj)
		}
	}
	return result, nil
}

func extStrComp(expected, observed string, useRegExp bool) (bool, error) {
	if useRegExp {
		return regexp.MatchString(expected, observed)
	}
	return expected == observed, nil
}

func gvkTest(expected, observed string) bool {
	if expected == "" {
		/* Skip the test */
		return true
	}
	return strings.EqualFold(expected, observed)
}

func decodeObject(data string) (client.Object, error) {
	rtObj, _, err := decoder.Decode([]byte(data), nil, nil)
	if err != nil {
		return nil, err
	}
	ctObj, isClientObj := rtObj.(client.Object)
	if !isClientObj {
		return nil, nil
	}
	return ctObj, nil
}
