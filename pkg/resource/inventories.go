package resource

import (
	"sync"

	"github.com/pkg/errors"
)

// Inventory is a collection of Kubernetes objects that an Operator uses. Each
// type of inventory is responsible for sourcing its objects.
//
// In order to do this, an inventory may consume the values that are provided
// by a `ValueProvider` (e.g. using `ValueProvider.Values`).
//
// An inventory implementation may use caching to improve performance. It can
// manipulating objects while being loaded subject to certain limitations.
// However, once the objects are loaded (using `Load` method) they must not
// change. Any change requires reloading the inventory.
//
// The implementation must assume that the inventory can be accessed from
// multiple calls or different threads. Therefore, it must guarantee the
// integrity of the inventory across multiple calls and explain the conditions
// for and limitations of manipulating objects. Note that the inventory does
// not have to be thread-safe and the caller must not assume so.
type Inventory interface {
	// Load returns the list of inventory objects, or an error, or both. It may
	// consult the specified `ValueProvider` to load the objects.
	//
	// Depending on the implementation the inventory may filter or manipulate
	// the objects while they are being loaded. However, once the list of
	// objects is returned its elements must not change.
	Load(provider ValueProvider) (Objects, error)
}

// Inventories is an alias for a list of inventories.
type Inventories []Inventory

// StaticInventory is a list programmatically created objects that are static
// and never change.
//
// Manipulating the objects of this type of inventory is not recommended. But
// if they do change it will impact all the accessors equally.
type StaticInventory = Objects

// Load returns the pre-defined objects which is the inventory itself. It
// never returns an error.
func (i StaticInventory) Load(provider ValueProvider) (Objects, error) {
	return i, nil
}

// DynamicInventory is a list programmatically created objects that can be
// manipulated by a chain of editors.
//
// This type of inventory guarantees that the original objects remain intact
// while the changes to it are scoped to the individual changes of the
// associated Custom Resource (which is adapted by `ValueProvider`).
//
// It uses `ValueProvider.Hash` to detect these changes. In other word, each
// hash value has its own copy of objects. Different instances and versions of
// a Custom Resource ought to result in different hash values.
type DynamicInventory struct {
	// Objects is a list of programmatically created objects.
	Objects Objects

	// Editors is a chain of `ObjectEditor`s that are applied one-by-one on
	// each object once the inventory is being loaded (using `Load` method).
	// The overall change on an object is the summation of individual changes
	// that each editor applies to it.
	Editors ObjectEditors

	kernel *dynamicInventory
	mutex  sync.Mutex
}

// Load makes a copy of the original objects, applies the chain of editors, and
// returns the changed objects. If an editor returns an error (any error other
// than object rejection) the method breaks and returns the error.
func (i *DynamicInventory) Load(provider ValueProvider) (Objects, error) {
	i.mutex.Lock()
	if i.kernel == nil {
		i.kernel = newDynamicInventory(i.Objects, i.Editors)
	}
	i.mutex.Unlock()

	cacheKey := provider.Hash()
	objects := i.kernel.get(cacheKey)
	if objects == nil {
		if objects, err := i.kernel.edit(provider); err != nil {
			return objects, err
		}
		i.kernel.set(cacheKey, objects)
	}
	return objects, nil
}

// ChartFilters is an alias for `ObjectPredicates`. It is used for filtering
// objects that are loaded from a Helm Chart.
type ChartFilters = ObjectPredicates

// ChartInventory is an inventory that is loaded from rendering a Helm Chart.
// It uses Helm SDK to find, load, and render a chart.
//
// It requires the path of the chart. When the path is relative, the inventory
// tries current directory and the globally configured chart directory (see
// `SetChartDirectory`) to locate the specified chart. It does not support
// discovery and downloading charts. The chart can be a directory or a `tar.gz`
// bundle.
//
// Once the chart is located and loaded, it uses the values from `ValueProvider`
// to render the chart. It uses the name and namespace of the underlaying
// Custom Resource as the release name and namespace of the chart.
//
// When the chart is rendered successfully it filters it objects based on the
// specified inclusion and exclusion criteria. For an object to pass the
// selection filter it must match none of the exclusion criteria and match any
// of the inclusion criteria. The exclusion criteria takes precedence. By
// default, when no criterion is specified, all objects pass.
//
// Since the Helm Chart rendering is computationally expensive, this inventory
// uses caching to speed up the process and avoid redundant operations. It uses
// `ValueProvider.Hash` to determine if the Helm Chart requires rendering.
type ChartInventory struct {
	// Path to the chart. It can be a directory or a `tar.gz` bundle that
	// contains a Helm Chart.
	Path string

	// Include is the list of object inclusion criteria.
	Include ChartFilters

	// Exclude is the list of object exclusion criteria.
	Exclude ChartFilters

	kernel *chartInventory
	mutex  sync.Mutex
}

// Load locates the Helm Chart and renders is with the values that are provided
// by `ValueProvider`. It uses the Helm-like values as-is and uses name and
// namespace of the underlaying Custom Resource as the release name and
// namespace of the chart.
//
// The output of the chart is filtered with the specified inclusion and
// exclusion criteria. For an object to pass this filter it must match none of
// the exclusion criteria and match any of the inclusion criteria. The exclusion
// criteria takes precedence. By default, when no criterion is specified, all
// objects pass the filter.
//
// This method fails if it fails to locate, render, or parse the output of the
// chart and return an error.
func (i *ChartInventory) Load(provider ValueProvider) (Objects, error) {
	i.mutex.Lock()
	if i.kernel == nil {
		i.kernel = newChartInventory(i.Path, filterChartObjects(i.Include, i.Exclude))
	}

	cacheKey := provider.Hash()
	objects := i.kernel.get(cacheKey)
	if objects == nil {
		if objects, err := i.kernel.render(provider); err != nil {
			return objects, err
		}
		i.kernel.set(cacheKey, objects)
	}
	return objects, nil
}

// AggregateInventory is an aggregator of other inventories. It facilitates
// working with multiple inventories while not being concerned about specific
// inventory types.
type AggregateInventory = Inventories

// Load iterates over all inventories and loads them one-by-one. It consolidates
// the objects that each individual inventory returns and returns them in one
// list. If any inventory returns an error it fails and returns the error.
//
// Note that this inventory does not do any validation or processing over the
// consolidated object list and has no insight into its content. Therefore you
// may have syntactic or semantic duplicates in the list.
func (i AggregateInventory) Load(provider ValueProvider) (Objects, error) {
	result := Objects{}
	for _, inv := range i {
		objects, err := inv.Load(provider)
		if err != nil {
			return result, err
		}
		result = append(result, objects...)
	}
	return result, nil
}

/* TODO: document */
type InventoryTable map[string]Inventory

/* TODO: document */
type InventorySelector func(provider ValueProvider) (string, error)

/* TODO: document */
type SelectableInventory struct {
	Table    InventoryTable
	Selector InventorySelector
}

/* TODO: document */
func (i *SelectableInventory) Load(provider ValueProvider) (Objects, error) {
	key, err := i.Selector(provider)
	if err != nil {
		return nil, err
	}

	inventory, hasKey := i.Table[key]
	if !hasKey {
		return nil, errors.Errorf("can not find an inventory for key %s", key)
	}

	return inventory.Load(provider)
}
