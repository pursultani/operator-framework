package resource

import (
	"fmt"

	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Objects is a container for Kubernetes objects. The only constraint is that
// the objects must be both serializable (implements `runtime.Object`) and
// identifiable (implements `metav1.Object`). Most built-in and custom objects
// match this constraint.
type Objects []client.Object

// DeepCopy makes a clone of Objects by invoking `DeepCopyObject` on individual
// objects.
//
// You should consider the efficiency of this method before using it.
func (o Objects) DeepCopy() Objects {
	clone := make(Objects, len(o))

	for idx, obj := range o {
		rtObj := obj.DeepCopyObject()

		ctObj, isClientObj := rtObj.(client.Object)
		if !isClientObj {
			/*
			 * This should never ever happen, but if/when it does then you
			 * must really panic!
			 */
			panic(
				fmt.Sprintf(
					"error at index %d of slice %p: expected %T, found %T",
					idx, o, ctObj, obj))
		}

		clone[idx] = ctObj
	}

	return clone
}

/* TODO: document */
type ObjectEditor func(object client.Object, provider ValueProvider) error

/* TODO: document */
type ObjectPredicate func(object client.Object, provider ValueProvider) (bool, error)

/* TODO: document */
type ObjectEditors []ObjectEditor

/* TODO: document */
type ObjectPredicates []ObjectPredicate

/* TODO: document */
func RejectObject(object client.Object) error {
	return errorRejectObject{
		object: object,
	}
}
