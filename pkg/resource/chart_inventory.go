package resource

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/patrickmn/go-cache"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/chartutil"
	"helm.sh/helm/v3/pkg/cli"
	"helm.sh/helm/v3/pkg/releaseutil"
)

/* ChartInventory implementation */

type chartInventory struct {
	cache         *cache.Cache
	filter        ObjectPredicate
	path          string
	envSettings   *cli.EnvSettings
	storageDriver string
	debugLogger   action.DebugLog
	disableHooks  bool
}

const (
	memoryStorageDriver = "memory"
)

var (
	noopLogger = func(_ string, _ ...interface{}) {}
)

func newChartInventory(path string, filter ObjectPredicate) *chartInventory {
	return &chartInventory{
		cache:         cache.New(cfgInventoryCacheTTL, cfgInventoryCacheTTL),
		filter:        filter,
		path:          path,
		envSettings:   cli.New(),
		storageDriver: memoryStorageDriver,
		debugLogger:   noopLogger,
		disableHooks:  cfgChartDisableHooks,
	}
}

func (i *chartInventory) get(cacheKey string) Objects {
	cached, isCached := i.cache.Get(cacheKey)
	if isCached && cached != nil {
		result, isObjects := cached.(Objects)
		if isObjects {
			return result
		}
	}
	return nil
}

func (i *chartInventory) set(cacheKey string, objects Objects) {
	i.cache.Set(cacheKey, objects, cache.DefaultExpiration)
}

func (i *chartInventory) render(provider ValueProvider) (Objects, error) {
	actionConfig := new(action.Configuration)
	if err := actionConfig.Init(i.envSettings.RESTClientGetter(),
		provider.Name().Namespace, i.storageDriver, i.debugLogger); err != nil {

		return nil, err
	}

	client := action.NewInstall(actionConfig)
	client.ClientOnly = true
	client.DryRun = true
	client.DisableHooks = i.disableHooks
	client.Replace = true
	client.Namespace = provider.Name().Namespace
	client.ReleaseName = provider.Name().Name
	if cfgChartKubeVersion != "" {
		kubeVersion, err := chartutil.ParseKubeVersion(cfgChartKubeVersion)
		if err != nil {
			return nil, err
		}
		client.KubeVersion = kubeVersion
	}
	if len(cfgChartAPIVersions) > 0 {
		client.APIVersions = cfgChartAPIVersions
	}

	chartDirs := []string{".", cfgChartDirectory}
	chartPath := i.path
	if !filepath.IsAbs(chartPath) {
		for _, dir := range chartDirs {
			chartPath = filepath.Join(dir, i.path)
			if _, err := os.Stat(chartPath); err == nil {
				break
			}
		}
	}

	chart, err := loader.Load(chartPath)
	if err != nil {
		return nil, err
	}

	release, err := client.Run(chart, provider.Values())
	if err != nil {
		return nil, err
	}

	manifests := releaseutil.SplitManifests(release.Manifest)
	if !i.disableHooks {
		for idx, hook := range release.Hooks {
			manifests[fmt.Sprintf("hook-%d", idx)] =
				fmt.Sprintf("# Hook: %s\n%s\n", hook.Path, hook.Manifest)
		}
	}

	result := make(Objects, 0, estimateCapacity(len(manifests)))

	for _, yaml := range manifests {
		obj, err := decodeObject(yaml)
		if err != nil {
			return result, err
		}
		include, err := i.filter(obj, provider)
		if err != nil {
			return result, err
		}
		if include {
			result = append(result, obj)
		}
	}

	return result, nil
}
