package context

import (
	"context"
	gocontext "context"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type ContextBuilder interface {
	WithClient(client client.Client) ContextBuilder
	WithRequest(request reconcile.Request) ContextBuilder
	Complete(parent gocontext.Context) gocontext.Context
}

func NewContextBuilder() ContextBuilder {
	return &ExecutionContext{}
}

func (c *ExecutionContext) WithClient(client client.Client) ContextBuilder {
	c.Client = client
	return c
}

func (c *ExecutionContext) WithRequest(request reconcile.Request) ContextBuilder {
	c.Request = request
	return c
}

func (c *ExecutionContext) Complete(parent context.Context) context.Context {
	return BuildContext(parent, WithClient(c.Client), WithRequest(c.Request))
}
