package context

import (
	gocontext "context"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type ContextOption = func(ctx gocontext.Context)

func WithClient(client client.Client) ContextOption {
	return func(ctx gocontext.Context) {
		// TODO
	}
}

func WithRequest(request reconcile.Request) ContextOption {
	return func(ctx gocontext.Context) {
		// TODO
	}
}
