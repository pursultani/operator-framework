package context

import (
	"context"
	gocontext "context"

	"github.com/go-logr/logr"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type ExecutionContext struct {
	gocontext.Context

	Client  client.Client
	Request reconcile.Request
	Logger  logr.Logger
	//Inventory resource.Inventory
	//Adapter resource.AdapterGenerator
}

// type (
// 	executionContextPayload ExecutionContext
// 	executionContextKeyType struct{}
// )

// var (
// 	executionContextKey executionContextKeyType = executionContextKeyType{}
// )

func FromContext(ctx gocontext.Context) ExecutionContext {
	// TODO:
	return ExecutionContext{}
}

func BuildContext(parent context.Context, options ...ContextOption) context.Context {
	// TODO:
	return parent
}
