OUTPUT_DIR = .out

GO = go
GO_LINT = golangci-lint
GO_COMPILER ?= gc

.DEFAULT_GOAL = all

.PHONY: all prepare build clean

all: prepare lint build clean

prepare:
	mkdir -p $(OUTPUT_DIR)

lint:
	$(GO_LINT) run 

build: prepare
	$(GO) build -compiler $(GO_COMPILER) ./alias.go

clean:
	rm -rf $(OUTPUT_DIR)
