module gitlab.com/pursultani/operator-framework

go 1.16

require (
	github.com/docker/spdystream v0.0.0-20160310174837-449fdfce4d96 // indirect
	github.com/go-logr/logr v0.4.0
	github.com/go-openapi/spec v0.19.3 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	helm.sh/helm/v3 v3.7.1
	k8s.io/apimachinery v0.22.2
	k8s.io/kubectl v0.22.2
	sigs.k8s.io/controller-runtime v0.10.2

)
